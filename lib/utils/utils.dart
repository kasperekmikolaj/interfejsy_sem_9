import 'dart:convert';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:interfejsy/utils/sing_in.dart';

import '../models/user.dart';
import 'constants.dart';

class Utils {
  static getChatId({firstUserId: String, secondUserId: String}) {
    int compareToResult = firstUserId.compareTo(secondUserId);

    String chatId = compareToResult < 0
        ? firstUserId + secondUserId
        : secondUserId + firstUserId;
    return chatId;
  }

  static Future<User> getCurrentUserFutureFromSecureStorage() async {
    String currentUserJson =
        await secureStorage.read(key: kCurrentUserKey) ?? "null";
    return User.fromJsonSecureStorage(jsonDecode(currentUserJson));
  }

  static getIdForFirebase(String userId) {
    String time = Timestamp.now().toString();
    String someInt = Random().nextInt(10000000).toString();
    return someInt + '_' + time + '_' + userId;
  }

/*  static String getRandomImage() {
    List<String> randomImages = [
      "assets/categories/ciastka.jpg",
      "assets/categories/ciasto.jpg",
      "assets/categories/grill.jpg",
      "assets/categories/koktajl.jpg",
      "assets/categories/pasta.jpg",
      "assets/categories/pizza.jpg",
      "assets/categories/przekaski.jpg",
      "assets/categories/przetwory.jpg",
      "assets/categories/salatka.jpg",
      "assets/categories/sniadanie.jpg",
      "assets/categories/zupa.jpg",
    ];

    Random random = Random.secure();
    int randomIndex = random.nextInt(randomImages.length);
    return randomImages[randomIndex];
  }*/
}
