import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:interfejsy/models/user.dart' as my;
import 'package:interfejsy/screens/main_screen.dart';
import 'package:interfejsy/utils/utils.dart';

import 'constants.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = GoogleSignIn();
final FlutterSecureStorage secureStorage = FlutterSecureStorage();
User? user;

Future<my.User> signInWithGoogle({required BuildContext context}) async {
  final GoogleSignInAccount? googleSignInAccount = await _googleSignIn.signIn();

  if (googleSignInAccount != null) {
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    try {
      // final UserCredential userCredential =
      await _auth.signInWithCredential(credential);
      // user = userCredential.user;
      user = FirebaseAuth.instance.currentUser;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'account-exists-with-different-credential') {
        ScaffoldMessenger.of(context).showSnackBar(
          customSnackBar(
            content: 'The account already exists with a different credential.',
          ),
        );
      } else if (e.code == 'invalid-credential') {
        ScaffoldMessenger.of(context).showSnackBar(
          customSnackBar(
            content: 'Error occurred while accessing credentials. Try again.',
          ),
        );
      }
    } catch (e) {
      print("error:");
      print(e);
      ScaffoldMessenger.of(context).showSnackBar(
        customSnackBar(
          content: 'Error occurred using Google Sign-In. Try again.',
        ),
      );
    }
  }
  if (user != null) {
    String json = jsonEncode({
      'id': user!.uid,
      'displayName': user!.displayName,
      'email': user!.email,
      'photoUrl': user!.photoURL,
    });

    secureStorage.write(key: kCurrentUserKey, value: json);

    CollectionReference usersCollection = firestore.collection('users');
    DocumentSnapshot userData = await usersCollection.doc(user!.uid).get();

    if (!userData.exists) {
      usersCollection.doc(user!.uid).set(
            ({
              'email': user!.email,
              'displayName': user!.displayName,
              'photoUrl': user!.photoURL
            }),
          );
    }
  }
  return Utils.getCurrentUserFutureFromSecureStorage();
}

SnackBar customSnackBar({required String content}) {
  return SnackBar(
    backgroundColor: Colors.black,
    content: Text(
      content,
      style: TextStyle(color: Colors.redAccent, letterSpacing: 0.5),
    ),
  );
}

Future<my.User> signOutGoogle() async {
  secureStorage.delete(key: kCurrentUserKey);
  await _googleSignIn.signOut();
  await _auth.signOut();
  return Utils.getCurrentUserFutureFromSecureStorage();
}
