import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:interfejsy/models/dish.dart';
import 'package:interfejsy/models/favorite_dish.dart';
import 'package:interfejsy/models/ingridniet.dart';
import 'package:interfejsy/models/ratings.dart';
import 'package:interfejsy/utils/test_image_urls.dart';

import 'constants.dart';

var ingredients = [
  Ingredient("1", "szklanka", "mleko"),
  Ingredient("2", "szklanka", "woda"),
  Ingredient("3", "szczypta", "sól"),
  Ingredient("4", "szczypta", "cukier"),
  Ingredient("5", "2 łyżki", "mąka"),
  Ingredient("6", "łyżka", "olej"),
  Ingredient("7", "jedeno", "ziele angielskie"),
  Ingredient("8", "200 gram", "masło"),
  Ingredient("9", "kilo", "czekolada"),
];

var dishes = [
  Dish(
      '1',
      "bananik",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Główne",
        "Owoce",
        "Wegańskie",
        "Śniadanie",
        "Amerykańska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image1,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '2',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Mączne",
        "Warzywa",
        "Wegetariańskie",
        "Przekąski",
        "Bałkańska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image2,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '3',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Kanapki",
        "Grzyby",
        "Mięsne",
        "Obiad",
        "Chińska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image3,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '4',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Makarony",
        "Mięso",
        "Mięsne",
        "Kolacja",
        "Grecka",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image4,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '5',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Naleśniki",
        "Ryby",
        "Wegetariańskie",
        "Lunch",
        "Polska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image5,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '6',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Pizze",
        "Owoce morza",
        "Wegetariańskie",
        "Śniadanie",
        "Indyjska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image6,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '7',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Niemiecka",
        "Lunch",
        "Wegańskie",
        "Makaron",
        "Zapiekanki",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image7,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '8',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Zupy",
        "Jajka",
        "Wegetariańskie",
        "Kolacja",
        "Tajska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image8,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '9',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Turecka",
        "Lunch",
        "Wegańskie",
        "Kasza",
        "Sosy",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image9,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '10',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Mączne",
        "Nabiał",
        "Wegańskie",
        "Kolacja",
        "Indyjska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image10,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '11',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Orientalna",
        "Kolacja",
        "Wegetariańskie",
        "Nabiał",
        "Sosy",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image11,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  Dish(
      '12',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Makarony",
        "Jajka",
        "Wegańskie",
        "Obiad",
        "Meksykańska",
      ],
      '24 sekundy',
      [
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
        Rating('ocena dania tralalalaal', 3, "autor", "1"),
      ],
      image1,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
];

var favoriteDishes = [
  FavoriteDish(
      '1',
      "ulubiony gyrosik",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Główne",
        "Owoce",
        "Wegańskie",
        "Śniadanie",
        "Amerykańska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image1,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '2',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Makarony",
        "Jajka",
        "Wegańskie",
        "Obiad",
        "Meksykańska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image2,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '3',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Orientalna",
        "Kolacja",
        "Wegetariańskie",
        "Nabiał",
        "Sosy",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image3,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '4',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Mączne",
        "Nabiał",
        "Wegańskie",
        "Kolacja",
        "Indyjska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image4,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '5',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Turecka",
        "Lunch",
        "Wegańskie",
        "Kasza",
        "Sosy",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image5,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '6',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Zupy",
        "Jajka",
        "Wegetariańskie",
        "Kolacja",
        "Tajska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image6,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '7',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Niemiecka",
        "Lunch",
        "Wegańskie",
        "Makaron",
        "Zapiekanki",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image7,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '8',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Pizze",
        "Owoce morza",
        "Wegetariańskie",
        "Śniadanie",
        "Indyjska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image8,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '9',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Naleśniki",
        "Ryby",
        "Wegetariańskie",
        "Lunch",
        "Polska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image9,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '10',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Makarony",
        "Mięso",
        "Mięsne",
        "Kolacja",
        "Grecka",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image10,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '11',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Kanapki",
        "Grzyby",
        "Mięsne",
        "Obiad",
        "Chińska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image11,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
  FavoriteDish(
      '12',
      "gyros",
      "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły,dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, dory gyrosik bałdzo dobły, "
          "no",
      [
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
        'robisz gyrosika i cacy',
      ],
      [
        "Mączne",
        "Warzywa",
        "Wegetariańskie",
        "Przekąski",
        "Bałkańska",
      ],
      '24 sekundy',
      'to jest notatka na temat mojego ulubionego dania tralalalala',
      image1,
      [
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
        Ingredient("1", "kilo", "mąki"),
      ]),
];

populateFirestoreWithData(String userId) {
  for (int i = 0; i < dishes.length; i++) {
    var dish = dishes[i];
    dishFirestoreRef.doc(dish.id).set(dish);
  }

  var favoriteDishFirestoreRef = FirebaseFirestore.instance
      .collection('users')
      .doc(userId)
      .collection('favoriteDishes')
      .withConverter<FavoriteDish>(
        fromFirestore: (snapshot, _) =>
            FavoriteDish.fromJsonFirebase(snapshot.data()!),
        toFirestore: (dish, _) => dish.toJson(),
      );
  for (int i = 0; i < favoriteDishes.length; i++) {
    var favoriteDish = favoriteDishes[i];
    favoriteDishFirestoreRef.doc(favoriteDish.id).set(favoriteDish);
  }
}
