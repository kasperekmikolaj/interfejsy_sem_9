import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interfejsy/models/abstract_dish.dart';
import 'package:interfejsy/models/dish.dart';

const String kCurrentUserKey = 'currentUser';
const String kFavoriteDishesScreenRoute = "/favorites";
final FirebaseFirestore firestore = FirebaseFirestore.instance;
final dishFirestoreRef = FirebaseFirestore.instance.collection('dishes').withConverter<Dish>(
  fromFirestore: (snapshot, _) => Dish.fromJsonFirebase(snapshot.data()!),
  toFirestore: (dish, _) => dish.toJson(),
);

var kNormalButtonBoxDecoration = BoxDecoration(
  color: Colors.blue,
  borderRadius: BorderRadius.circular(30),
  boxShadow: kElevationToShadow[4],
);

var kInfoScreenBoxDecoration = const BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    colors: [Colors.white, Colors.lightBlueAccent],
    // colors: [Colors.cyanAccent[100], Colors.blue[900]],
  ),
);

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

InputDecoration kMessageTextFieldDecoration({hintText: String}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    hintText: hintText,
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
    fillColor: Colors.black12,
    filled: true,
  );
}

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.black, width: 2.0),
  ),
);

