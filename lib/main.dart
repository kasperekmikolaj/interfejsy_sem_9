import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:interfejsy/screens/main_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return _getMaterialApp();
  }

  _getMaterialApp() {
    Firebase.initializeApp();
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Raleway', primarySwatch: Colors.blue),
      title: 'Item list',
      home: MainScreen(),
    );
  }
}
