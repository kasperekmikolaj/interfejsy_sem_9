import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/screens/profile_info_screen.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/sing_in.dart';
import 'package:interfejsy/utils/utils.dart';

import 'buttons/my_circle_button.dart';
import 'loading_screen.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar(
    this.text,
    this.barColor,
  )   : preferredSize = Size.fromHeight(kToolbarHeight),
        super();

  @override
  final Size preferredSize; // default is 56.0
  final String text;
  final Color barColor;

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(widget.text),
      backgroundColor: widget.barColor,
      // leading: Container(), // no back button
      actions: <Widget>[
        _avatarContainer(),

// fixme wyswietlanie awatara z googla na appbarze
 /*       FutureBuilder(
          future: Utils.getCurrentUserFutureFromSecureStorage(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data != null) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ProfileInfoScreen(
                          'Twój profil',
                          MyCircleButton(
                            'Sign Out',
                            () {
                              signOutGoogle().whenComplete(() => {
                                    setState(() {}),
                                    Navigator.of(context).popUntil(
                                      ModalRoute.withName('/'),
                                    )
                                  });
                            },
                            Colors.deepPurple,
                            Colors.white,
                          ),
                        ),
                      ),
                    );
                  },
                  child: _avatarContainer(),
                );
              } else {
                return GestureDetector(
                  onTap: () {
                    signInWithGoogle(context: context)
                        .whenComplete(() => setState(() {}));
                  },
                  child: _avatarContainer(),
                );
              }
            } else {
              return LoadingScreen();
            }
          },
        ),*/
      ],
    );
  }

  static _avatarContainer() {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 1, 25, 1),
      child: const CircleAvatar(
        radius: 28,
        backgroundImage: AssetImage('assets/czapka.png'),
      ),
    );

    //fixme awatar na appbarze
  /*  return FutureBuilder(
      future: secureStorage.read(key: kCurrentUserKey),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          User currentUser =
              User.fromJsonSecureStorage(json.decode(snapshot.data as String));
          return CircleAvatar(
            radius: 28,
            backgroundImage: NetworkImage(currentUser.photoUrl),
          );
        } else {
          return const CircleAvatar(
            radius: 28,
            backgroundImage: AssetImage('assets/czapka.png'),
          );
        }
      },
    );*/
  }
}
