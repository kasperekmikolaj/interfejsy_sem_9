import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget roundedText(String text) {
  return Container(
    child: Text (
        text,
        textAlign: TextAlign.center,
        maxLines: 1,
        softWrap: false,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
            fontSize: 18,
        )
    ),
    decoration: BoxDecoration (
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        color: Colors.red
    ),
    padding: EdgeInsets.fromLTRB(12,8,12,8),
  );
}