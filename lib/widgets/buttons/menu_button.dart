import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/screens/favorite_dishes_screen.dart';
import 'package:interfejsy/screens/profile_info_screen.dart';
import 'package:interfejsy/screens/search_filter_screeen.dart';
import 'package:interfejsy/screens/main_screen.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/sing_in.dart';
import 'package:interfejsy/utils/test_data.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';
import 'package:interfejsy/widgets/loading_screen.dart';

class MenuButton extends StatefulWidget {
  const MenuButton(this.isMainScreen, {Key? key}) : super(key: key);

  final bool isMainScreen;

  @override
  _MenuButtonState createState() => _MenuButtonState();
}

class _MenuButtonState extends State<MenuButton> {
  final _homeIconColor = Colors.green;
  final _favouriteIconColor = Colors.redAccent;
  final _profileIconColor = Colors.lightBlueAccent;
  final _searchIconColor = Colors.indigo;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Utils.getCurrentUserFutureFromSecureStorage(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null) {
              if (!widget.isMainScreen) {
                return SpeedDial(
                  buttonSize: 70,
                  animatedIcon: AnimatedIcons.menu_close,
                  animatedIconTheme: const IconThemeData(size: 24),
                  backgroundColor: Colors.blue,
                  visible: true,
                  childrenButtonSize: 65,
                  curve: Curves.bounceIn,
                  children: [
                    SpeedDialChild(
                        child: const Icon(Icons.home),
                        backgroundColor: _homeIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).popUntil(
                            ModalRoute.withName('/'),
                          );
                        },
                        label: 'Ekran główny',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _homeIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.search),
                        backgroundColor: _searchIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SearchFilterScreen(),
                          ));
                        },
                        label: 'Filtruj',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _searchIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.favorite),
                        backgroundColor: _favouriteIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            settings: RouteSettings(name: kFavoriteDishesScreenRoute),
                            builder: (context) => FavoriteDishesScreen(),
                          ));
                        },
                        label: 'Ulubione',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _favouriteIconColor),
                  ],
                );
              } else {
                return SpeedDial(
                  buttonSize: 70,
                  animatedIcon: AnimatedIcons.menu_close,
                  animatedIconTheme: const IconThemeData(size: 24),
                  backgroundColor: Colors.blue,
                  visible: true,
                  childrenButtonSize: 65,
                  curve: Curves.bounceIn,
                  children: [
                    SpeedDialChild(
                        child: const Icon(Icons.search),
                        backgroundColor: _searchIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {

                          // fixme delete - populate database
                          // zalogowany guzik filtrowania
                          // var userDataSnap = snapshot.data as User;
                          // populateFirestoreWithData(userDataSnap.id);



                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SearchFilterScreen(),
                          ));
                        },
                        label: 'Filtruj',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _searchIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.favorite),
                        backgroundColor: _favouriteIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            settings: RouteSettings(name: kFavoriteDishesScreenRoute),
                            builder: (context) => FavoriteDishesScreen(),
                          ));
                        },
                        label: 'Ulubione',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _favouriteIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.account_circle),
                        backgroundColor: _profileIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => ProfileInfoScreen(
                                'Twój profil',
                                MyCircleButton(
                                  'Sign Out',
                                  () {
                                    signOutGoogle().whenComplete(() => {
                                          setState(() {}),
                                          Navigator.of(context).popUntil(
                                            ModalRoute.withName('/'),
                                          )
                                        });
                                  },
                                  Colors.deepPurple,
                                  Colors.white,
                                ),
                              ),
                            ),
                          );
                        },
                        label: 'Profil',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _profileIconColor)
                  ],
                );
              }
            } else {
              if (!widget.isMainScreen) {
                return SpeedDial(
                  buttonSize: 70,
                  animatedIcon: AnimatedIcons.menu_close,
                  animatedIconTheme: const IconThemeData(size: 24),
                  backgroundColor: Colors.blue,
                  visible: true,
                  childrenButtonSize: 65,
                  curve: Curves.bounceIn,
                  children: [
                    SpeedDialChild(
                        child: const Icon(Icons.home),
                        backgroundColor: _homeIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).popUntil(
                            ModalRoute.withName('/'),
                          );
                        },
                        label: 'Ekran główny',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _homeIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.search),
                        backgroundColor: _searchIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SearchFilterScreen(),
                          ));
                        },
                        label: 'Filtruj',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _searchIconColor),
                  ],
                );
              } else {
                return SpeedDial(
                  buttonSize: 70,
                  animatedIcon: AnimatedIcons.menu_close,
                  animatedIconTheme: const IconThemeData(size: 24),
                  backgroundColor: Colors.blue,
                  visible: true,
                  childrenButtonSize: 65,
                  curve: Curves.bounceIn,
                  children: [
                    SpeedDialChild(
                        child: const Icon(Icons.search),
                        backgroundColor: _searchIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => SearchFilterScreen(),
                          ));
                        },
                        label: 'Filtruj',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _searchIconColor),
                    SpeedDialChild(
                        child: const Icon(Icons.account_circle),
                        backgroundColor: _profileIconColor,
                        foregroundColor: Colors.white,
                        onTap: () {
                          signInWithGoogle(context: context)
                              .whenComplete(() => setState(() {}));
                        },
                        label: 'Logowanie google',
                        labelStyle: const TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 16.0),
                        labelBackgroundColor: _profileIconColor)
                  ],
                );
              }
            }
          } else {
            return LoadingScreen();
          }
        });
  }
}
