import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyCircleButton extends StatelessWidget {
  final Function onPressedFunction;
  final String text;
  final Color primaryColor;
  final Color textColor;

  MyCircleButton(
      this.text, this.onPressedFunction, this.primaryColor, this.textColor);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressedFunction();
      },
      style: ElevatedButton.styleFrom(
        primary: primaryColor,
        onPrimary: textColor,
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          text,
          style: TextStyle(fontSize: 25, color: textColor),
        ),
      ),
    );
  }
}
