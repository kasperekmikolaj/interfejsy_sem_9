import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/user.dart';

import 'buttons/my_circle_button.dart';

class UserInfoColumn extends StatelessWidget {
  final User user;
  final MyCircleButton button;

  UserInfoColumn({required this.user, required this.button});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        const SizedBox(
          height: 5,
        ),
        CircleAvatar(
          backgroundImage: NetworkImage(user.photoUrl),
          radius: 60,
          backgroundColor: Colors.transparent,
        ),
        const SizedBox(height: 30),
        const Text(
          'NAME',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Text(
          user.displayName,
          style: const TextStyle(
            fontSize: 25,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 20),
        const Text(
          'EMAIL',
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        Text(
          user.email,
          style: const TextStyle(
            fontSize: 25,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 40),
        button
      ],
    );
  }
}
