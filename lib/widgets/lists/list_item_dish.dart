import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/abstract_dish.dart';
import 'package:interfejsy/screens/dish_details_screen.dart';

class ListItemDish extends StatefulWidget {

  final bool isFavoriteDish;
  final AbstractDish dish;

  ListItemDish(this.isFavoriteDish, this.dish);

  @override
  _ListItemDish createState() => _ListItemDish();
}

class _ListItemDish extends State<ListItemDish> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => DishDetailsScreen(widget.dish.id, widget.isFavoriteDish),
            ),
          ).then((value) => setState((){}));
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(),
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: const EdgeInsets.fromLTRB(15.0, 10.0, 5.0, 10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(widget.dish.photoUrl),
                ),
                flex: 1,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(15, 1, 0, 1),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.dish.name,
                        textScaleFactor: 1.1,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                ),
                flex: 5,
              )
            ],
          ),
        ),
      ),
    );
  }
}