import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'list_item_dish.dart';

class MyCustomList extends StatefulWidget {
  List dishesList;
  String header;
  bool isFavoriteDish;
  Color headerColor;

  MyCustomList(this.dishesList, this.header,
      {this.isFavoriteDish = false, this.headerColor = Colors.blue});

  @override
  _MyCustomList createState() => _MyCustomList();
}

class _MyCustomList extends State<MyCustomList> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Card(
          color: widget.headerColor,
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    widget.header,
                    style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        ListView.builder(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: widget.dishesList.isEmpty ? 1 : widget.dishesList.length,
          itemBuilder: (context, index) {
            if (widget.dishesList.isEmpty) {
              String title = widget.isFavoriteDish
                  ? "Żaden przepis nie został jeszcze polubiony"
                  : "W systemie nie ma jeszcze przepisów";
              return Padding(
                padding: const EdgeInsets.fromLTRB(0, 200, 0, 0),
                child: ListTile(
                  title: Container(
                    decoration: BoxDecoration(
                      color: Colors.red,
                      border: Border.all(),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Text(
                              title,
                              textAlign: TextAlign.center,
                              textScaleFactor: 3,
                            ),
                          ),
                          flex: 3,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            } else {
              return ListItemDish(
                  widget.isFavoriteDish, widget.dishesList[index]);
            }
          },
        ),
      ],
    );
  }
}
