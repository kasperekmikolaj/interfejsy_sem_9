import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/ratings.dart';

String getAssetPath(int starNumber) {
  return "assets/" + starNumber.toString() + ".png";
}

Widget createRatingItem(BuildContext context, Rating rating) {
  return ListTile(
    title: InkWell(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(),
          borderRadius: BorderRadius.circular(8.0),
        ),
        padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    rating.text,
                    textAlign: TextAlign.start,
                  ),
                  flex: 4,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 1, 0, 1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Image(
                          image: AssetImage(getAssetPath(rating.stars)),
                        ),
                      ],
                    ),
                  ),
                  flex: 1,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 5),
                  child: Text(
                    "Autor: " + rating.authorName,
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}
