import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:interfejsy/models/abstract_dish.dart';
import 'package:interfejsy/models/dish.dart';
import 'package:interfejsy/models/favorite_dish.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/screens/change_notes_screen.dart';
import 'package:interfejsy/screens/favorite_dishes_screen.dart';
import 'package:interfejsy/screens/opinion_screen.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';
import 'package:interfejsy/widgets/labels/rounded_text.dart';
import 'package:interfejsy/widgets/lists/create_list_item.dart';
import 'package:interfejsy/widgets/loading_screen.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

class DishDetailsScreen extends StatefulWidget {
  final String dishId;
  final bool isFavorite;

  DishDetailsScreen(this.dishId, this.isFavorite);

  @override
  _DishDetailsScreen createState() => _DishDetailsScreen();
}

class _DishDetailsScreen extends State<DishDetailsScreen> {
  bool showOpinionForm = false;
  late Future<User> _currentUserFuture;
  bool isAlreadyFavorite = true;

  final TextStyle _titleStyle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  final TextStyle _textStyle = const TextStyle(fontSize: 16);

  @override
  void initState() {
    super.initState();
    _currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
    _isFavoriteAlready();
  }

  _isFavoriteAlready() async {
    await _currentUserFuture.then(
      (value) {
        FirebaseFirestore.instance
            .collection('users')
            .doc(value.id)
            .collection('favoriteDishes')
            .doc(widget.dishId)
            .get()
            .then((value) => value.exists
                ? isAlreadyFavorite = true
                : isAlreadyFavorite = false);
      },
    );
  }

  Future<AbstractDish> _getDish() async {
    if (!widget.isFavorite) {
      DocumentReference dishDoc = dishFirestoreRef.doc(widget.dishId);
      return await dishDoc.get().then((snapshot) => snapshot.data() as Dish);
    } else {
      String userId = await _currentUserFuture.then((user) => user.id);
      var favoriteDishFirestoreRef = FirebaseFirestore.instance
          .collection('users')
          .doc(userId)
          .collection('favoriteDishes')
          .withConverter<FavoriteDish>(
            fromFirestore: (snapshot, _) =>
                FavoriteDish.fromJsonFirebase(snapshot.data()!),
            toFirestore: (dish, _) => dish.toJson(),
          );
      DocumentReference dishDoc = favoriteDishFirestoreRef.doc(widget.dishId);
      return await dishDoc
          .get()
          .then((snapshot) => snapshot.data() as FavoriteDish);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getDish(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          AbstractDish _dish = snapshot.data as AbstractDish;
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
              new TextEditingController().clear();
            },
            child: Scaffold(
              floatingActionButton: _floatingButton(_dish),
              appBar: CustomAppBar(
                  'Przepis na '+_dish.name, widget.isFavorite ? Colors.red : Colors.blue),
              body: ListView(
                children: [
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        ListTile(
                          title: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 6, 15, 10),
                            child: Text(
                              _dish.name,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                              ),
                            ),
                          ),
                          subtitle: Padding(
                            padding: EdgeInsets.fromLTRB(5, 6, 15, 10),
                            child: Text(
                              "Czas wykonania ~ " + _dish.preparationTime,
                              style: const TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 15, right: 15),
                          child: Image.network(
                            _dish.photoUrl,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(12),
                          child: Wrap(
                            runSpacing: 5,
                            spacing: 5,
                            children: List.generate(
                              _dish.categories.length,
                              (index) => roundedText(_dish.categories[index]),
                            ),
                          ),
                        ),
                        Divider(
                          indent: 20,
                          endIndent: 20,
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 10, 0),
                              child: Text(
                                "Opis:",
                                style: _titleStyle,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Text(
                                _dish.description,
                                style: _textStyle,
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          indent: 20,
                          endIndent: 20,
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 10, 15),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 15),
                                    child: Text(
                                      "Składniki:",
                                      style: _titleStyle,
                                    ),
                                  ),
                                ],
                              ),
                              ListView.separated(
                                shrinkWrap: true,
                                physics: const BouncingScrollPhysics(),
                                itemCount: _dish.ingredients.length,
                                separatorBuilder: (_, __) => Container(
                                    height: 1.5, color: Colors.grey[300]),
                                itemBuilder: (context, index) {
                                  return Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        15.0, 10, 5.0, 10.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            _dish.ingredients[index].name,
                                            style: _textStyle,
                                          ),
                                          flex: 3,
                                        ),
                                        Expanded(
                                          child: Text(
                                            _dish.ingredients[index].quantity,
                                            style: _textStyle,
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          indent: 20,
                          endIndent: 20,
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 10, 15),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 15),
                                    child: Text(
                                      "Plan wykonania:",
                                      style: _titleStyle,
                                    ),
                                  ),
                                ],
                              ),
                              ListView.builder(
                                shrinkWrap: true,
                                physics: const BouncingScrollPhysics(),
                                itemCount: _dish.preparationSteps.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10.0, 0, 5.0, 10.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border:
                                                Border.all(color: Colors.grey),
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          padding: EdgeInsets.all(14),
                                          child: Text(
                                            (1 + index).toString() +
                                                ". " +
                                                _dish.preparationSteps[index],
                                            style: _textStyle,
                                          ),
                                        )),
                                      ],
                                    ),
                                  );
                                },
                              ),
                              Divider(
                                endIndent: 10,
                                color: Colors.grey[300],
                                thickness: 2,
                              ),
                              _conditionalWidget(_dish),
                              Divider(
                                endIndent: 10,
                                color: Colors.grey[300],
                                thickness: 2,
                              ),
                              const Padding(
                                padding: EdgeInsets.fromLTRB(20, 35, 20, 70),
                                child: Text(
                                  "SMACZNEGO",
                                  style: TextStyle(
                                    fontSize: 36,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return LoadingScreen();
        }
      },
    );
  }

  _conditionalWidget(AbstractDish _dish) {
    var titlePaddingVal = const EdgeInsets.fromLTRB(0, 25, 0, 15);
    var contentPaddingVal = const EdgeInsets.fromLTRB(5, 0, 5, 15);
    if (widget.isFavorite) {
      FavoriteDish dish = _dish as FavoriteDish;
      return Column(
        children: [
          Row(
            children: [
              Padding(
                padding: titlePaddingVal,
                child: Text(
                  "Twoje uwagi:",
                  style: _titleStyle,
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: contentPaddingVal,
                child: Text(
                  dish.note == '' ? "Nie dodano żadnych uwag." : dish.note,
                  style: _textStyle,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ],
      );
    } else {
      Dish dish = _dish as Dish;
      return Column(
        children: [
          Row(
            children: [
              Padding(
                padding: titlePaddingVal,
                child: Text(
                  "Oceny użytkowników:",
                  style: _titleStyle,
                ),
              ),
            ],
          ),
          Column(
            children: [
              ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount: dish.ratings.length,
                itemBuilder: (context, index) {
                  return createRatingItem(context, dish.ratings[index]);
                },
              ),
            ],
          ),
          FutureBuilder(
            future: _currentUserFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data != null) {
                  User user = snapshot.data as User;
                  return Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: MyCircleButton("Dodaj ocenę", () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (builder) => OpinionFormScreen(user, _dish),
                        ),
                      );
                    }, Colors.green, Colors.white),
                  );
                } else {
                  return SizedBox.shrink();
                }
              } else {
                return SizedBox.shrink();
              }
            },
          )
        ],
      );
    }
  }

  _makeFavorite(Dish dish) async {
    String userId = await _currentUserFuture.then((user) => user.id);
    var favoriteDishFirestoreRef = FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .collection('favoriteDishes')
        .withConverter<FavoriteDish>(
          fromFirestore: (snapshot, _) =>
              FavoriteDish.fromJsonFirebase(snapshot.data()!),
          toFirestore: (dish, _) => dish.toJson(),
        );
    FavoriteDish favoriteDish = FavoriteDish.fromDish(dish);
    favoriteDishFirestoreRef.doc(dish.id).set(favoriteDish);
  }

  _deleteFavorite() async {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text("Usunąć z listy ulubionych?"),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Anuluj'),
          ),
          TextButton(
            onPressed: () {
              _deleteFavoriteFromDatabase();
              Navigator.of(context).popUntil(
                ModalRoute.withName('/'),
              );
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (builder) => FavoriteDishesScreen()));
            },
            child: const Text('Usuń'),
          ),
        ],
      ),
    );
  }

  _deleteFavoriteFromDatabase() async {
    String userId = await _currentUserFuture.then((user) => user.id);
    var favoriteDishFirestoreRef = FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .collection('favoriteDishes')
        .withConverter<FavoriteDish>(
          fromFirestore: (snapshot, _) =>
              FavoriteDish.fromJsonFirebase(snapshot.data()!),
          toFirestore: (dish, _) => dish.toJson(),
        );
    favoriteDishFirestoreRef.doc(widget.dishId).delete();
  }

  _editNotes(FavoriteDish favoriteDish) {
    Navigator.of(context)
        .push(
          MaterialPageRoute(
            builder: (builder) => ChangeNoteScreen(favoriteDish),
          ),
        )
        .then((value) => setState(() {}));
  }

  _floatingButton(AbstractDish _dish) {
    if (widget.isFavorite) {
      FavoriteDish favoriteDish = _dish as FavoriteDish;
      return SpeedDial(
        buttonSize: 70,
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: const IconThemeData(size: 24),
        backgroundColor: Colors.blue,
        visible: true,
        childrenButtonSize: 65,
        curve: Curves.bounceIn,
        children: [
          SpeedDialChild(
              child: const Icon(Icons.delete),
              backgroundColor: Colors.deepPurple,
              foregroundColor: Colors.white,
              onTap: () {
                _deleteFavorite();
              },
              label: 'Usuń z ulubionych',
              labelStyle: const TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontSize: 16.0),
              labelBackgroundColor: Colors.deepPurple),
          SpeedDialChild(
              child: const Icon(Icons.settings),
              backgroundColor: Colors.amber,
              foregroundColor: Colors.white,
              onTap: () {
                _editNotes(favoriteDish);
              },
              label: 'Edytuj notatki',
              labelStyle: const TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  fontSize: 16.0),
              labelBackgroundColor: Colors.amber),
        ],
      );
    } else {
      return FutureBuilder(
        future: _currentUserFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null) {
              return FloatingActionButton(
                onPressed: () => showDialog(
                  context: context,
                  builder: isAlreadyFavorite
                      ? (BuildContext context) => AlertDialog(
                            title: Text("Przepis został już zapisany"),
                            actions: [
                              TextButton(
                                onPressed: () => Navigator.pop(context),
                                child: const Text('Ok'),
                              ),
                            ],
                          )
                      : (BuildContext context) => AlertDialog(
                            title: Text("Zapisać w ulubionych przepisach?"),
                            actions: [
                              TextButton(
                                onPressed: () => Navigator.pop(context),
                                child: const Text('Anuluj'),
                              ),
                              TextButton(
                                onPressed: () => {
                                  _makeFavorite(_dish as Dish),
                                  Navigator.pop(context),
                                },
                                child: const Text('Zapisz'),
                              ),
                            ],
                          ),
                ),
                child: const Icon(
                  Icons.favorite,
                ),
                backgroundColor: Colors.redAccent,
              );
            } else {
              return SizedBox.shrink();
            }
          } else {
            return SizedBox.shrink();
          }
        },
      );
    }
  }
}
