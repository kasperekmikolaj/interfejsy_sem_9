import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/dish.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/menu_button.dart';
import 'package:interfejsy/widgets/lists/create_expandable_list.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

import 'loading_screen.dart';

class DishesScreen extends StatefulWidget {
  final List<String> categories;

  const DishesScreen(this.categories, {Key? key}) : super(key: key);

  @override
  _DishesScreen createState() => _DishesScreen();
}

class _DishesScreen extends State<DishesScreen> {
  late Future<User> currentUserFuture;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffcfdbff),
      appBar: CustomAppBar('Super kucharz!', Colors.blue),
      body: FutureBuilder(
        future: dishFirestoreRef
            .where('categories', arrayContainsAny: widget.categories)
            .get(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var querySnap = snapshot.data as QuerySnapshot<Dish>;
            List<Dish> dishes = querySnap.docs.map((e) => e.data()).toList();
            for (int j = 0; j < widget.categories.length; j++) {
              dishes.removeWhere((element) =>
                  !element.categories.contains(widget.categories[j]));
            }
            return MyCustomList(dishes, "Dania");
          } else {
            return LoadingScreen();
          }
        },
      ),
      floatingActionButton: MenuButton(false),
    );
  }
}
