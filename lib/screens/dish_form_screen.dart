import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:interfejsy/models/categories/categories_enum.dart';
import 'package:interfejsy/models/dish.dart';
import 'package:interfejsy/models/favorite_dish.dart';
import 'package:interfejsy/models/ingridniet.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

class DishFormScreen extends StatefulWidget {
  final String? dishId;

  DishFormScreen(this.dishId);

  @override
  _DishFormScreen createState() => _DishFormScreen();
}

// todo
// ządać podania pliku przed zapisem
// walidatory - nie pozwalać na zapis jak puste składniki / kroki itp

class _DishFormScreen extends State<DishFormScreen> {
  late Future<User> _currentUserFuture;
  final TextStyle _titleStyle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  final TextStyle _textStyle = const TextStyle(fontSize: 16);
  final EdgeInsets _standardPadding =
      const EdgeInsets.only(top: 10, bottom: 10);

  File? _newImage;

  final _mainFormKey = GlobalKey<FormState>();
  final _ingredientsFormKey = GlobalKey<FormState>();
  final _stepsFormKey = GlobalKey<FormState>();

  static final TextEditingController _dishNameController =
      TextEditingController();
  static final TextEditingController _descriptionController =
      TextEditingController();
  static final TextEditingController _preparationTimeController =
      TextEditingController();
  static final TextEditingController _newStepController =
      TextEditingController();
  static final TextEditingController _ingredientNameController =
      TextEditingController();
  static final TextEditingController _ingredientAmountController =
      TextEditingController();

  final List<Ingredient> _ingredientsDish = [];
  final List<String> _preparationStepsDish = [];

  List<DishTypeCategory> dishTypeCategoryList = DishTypeCategory.values;
  List<bool> dishTypeCategorySelections =
      DishTypeCategory.values.map((e) => false).toList();

  List<IngredientsCategory> ingredientCategoryList = IngredientsCategory.values;
  List<bool> ingredientCategorySelections =
      IngredientsCategory.values.map((e) => false).toList();

  List<MeatinessCategory> meatinessCategoryList = MeatinessCategory.values;
  List<bool> meatinessCategorySelections =
      MeatinessCategory.values.map((e) => false).toList();

  List<MealTimeCategory> mealTimeCategoryList = MealTimeCategory.values;
  List<bool> mealTimeCategorySelections =
      MealTimeCategory.values.map((e) => false).toList();

  List<OriginCategory> originCategoryList = OriginCategory.values;
  List<bool> originCategorySelections =
      OriginCategory.values.map((e) => false).toList();

  List<String> selectedCategories = [];

  @override
  void initState() {
    super.initState();
    _currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  String? validateTextField(String value,
      [String validationText = "To pole nie może być puste!"]) {
    print('halo walidacja: ' + validationText);
    if (value.isEmpty) {
      return validationText;
    }
    return null;
  }

  _getInputDecoration(String text) {
    return InputDecoration(
      border: OutlineInputBorder(),
      labelText: text,
    );
  }

  _addDishToFirebase() async {
    String userId = await _currentUserFuture.then((user) => user.id);

    String filename = _newImage!.path + Utils.getIdForFirebase(userId);

    FirebaseStorage.instance;
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('uploads/$filename');
    UploadTask uploadTask = firebaseStorageRef.putFile(_newImage!);
    TaskSnapshot taskSnapshot = await uploadTask;
    await taskSnapshot.ref.getDownloadURL().then((refUrl) {
      Dish dish = Dish(
          Utils.getIdForFirebase(userId),
          _dishNameController.value.text,
          _descriptionController.value.text,
          _preparationStepsDish,
          selectedCategories,
          _preparationTimeController.value.text,
          [],
          refUrl,
          _ingredientsDish);
      var favoriteDishFirestoreRef = FirebaseFirestore.instance
          .collection('users')
          .doc(userId)
          .collection('favoriteDishes')
          .withConverter<FavoriteDish>(
            fromFirestore: (snapshot, _) =>
                FavoriteDish.fromJsonFirebase(snapshot.data()!),
            toFirestore: (dish, _) => dish.toJson(),
          );
      FavoriteDish favoriteDish = FavoriteDish.fromDish(dish);
      favoriteDishFirestoreRef.doc(dish.id).set(favoriteDish);

      var newDishFirestoreRef = FirebaseFirestore.instance
          .collection('dishes')
          .withConverter<FavoriteDish>(
            fromFirestore: (snapshot, _) =>
                FavoriteDish.fromJsonFirebase(snapshot.data()!),
            toFirestore: (dish, _) => dish.toJson(),
          );
      FavoriteDish newDish = FavoriteDish.fromDish(dish);
      newDishFirestoreRef.doc(dish.id).set(newDish);
      _dishNameController.clear();
      _descriptionController.clear();
      _preparationTimeController.clear();
      _newStepController.clear();
      _ingredientNameController.clear();
      _ingredientAmountController.clear();
      selectedCategories = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        appBar: CustomAppBar("Stwórz nowy przepis!", Colors.blue),
        body: Padding(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: [
              Column(
                children: [
                  Form(
                    key: _mainFormKey,
                    child: Column(
                      children: [
                        // name
                        Padding(
                          padding: _standardPadding,
                          child: TextFormField(
                            controller: _dishNameController,
                            decoration: _getInputDecoration("Nazwa dania"),
                            validator: (value) =>
                                validateTextField(value!, 'Podaj nazwę dania'),
/*                            onSaved: (value) => setState(
                              () {
                                _nameDish = value!;
                              },
                            ),*/
                          ),
                        ),

                        // photo
                        Container(
                          alignment: Alignment.center,
                          child: _newImage != null
                              ? Image.file(
                                  _newImage!,
                                  fit: BoxFit.fill,
                                )
                              : Image.asset(
                                  "assets/add_photo.png",
                                  fit: BoxFit.fill,
                                ),
                        ),
                        Padding(
                          padding: _standardPadding,
                          child: MyCircleButton("Zmień zdjęcie", () {
                            _pickFile();
                          }, Colors.blue, Colors.white),
                        ),

                        // preparation time
                        Padding(
                          padding: _standardPadding,
                          child: TextFormField(
                            controller: _preparationTimeController,
                            decoration:
                                _getInputDecoration("Czas przygotowania"),
                            validator: (value) => validateTextField(
                                value!, 'Podaj czas przygotowania'),
/*                            onSaved: (value) => setState(
                              () {
                                _nameDish = value!;
                              },
                            ),*/
                          ),
                        ),

                        // description
                        Padding(
                          padding: EdgeInsets.only(top: 0, bottom: 10),
                          child: TextFormField(
                            controller: _descriptionController,
                            decoration: _getInputDecoration("Opis dania"),
                            validator: (value) =>
                                validateTextField(value!, 'Podaj opis dania'),
/*                            onSaved: (value) => setState(
                              () {
                                _nameDish = value!;
                              },
                            ),*/
                            minLines: 3,
                            maxLines: 6,
                          ),
                        ),

                        // ingredients
                        Padding(
                          padding: EdgeInsets.only(top: 30, bottom: 10),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 15),
                                    child: Text(
                                      "Składniki:",
                                      style: _titleStyle,
                                    ),
                                  ),
                                ],
                              ),
                              ListView.builder(
                                shrinkWrap: true,
                                physics: const BouncingScrollPhysics(),
                                itemCount: _ingredientsDish.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                    padding: const EdgeInsets.fromLTRB(
                                        15.0, 10.0, 5.0, 10.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            _ingredientsDish[index].name,
                                            style: _textStyle,
                                          ),
                                          flex: 3,
                                        ),
                                        Expanded(
                                          child: Text(
                                            _ingredientsDish[index].quantity,
                                            style: _textStyle,
                                          ),
                                          flex: 2,
                                        ),
                                        Expanded(
                                          child: Material(
                                            color: Colors.white,
                                            child: Center(
                                              child: Ink(
                                                decoration:
                                                    const ShapeDecoration(
                                                  color: Colors.red,
                                                  shape: CircleBorder(),
                                                ),
                                                child: IconButton(
                                                  icon:
                                                      const Icon(Icons.delete),
                                                  color: Colors.white,
                                                  onPressed: () {
                                                    setState(() {
                                                      _ingredientsDish
                                                          .removeAt(index);
                                                    });
                                                  },
                                                ),
                                                width: 40,
                                                height: 40,
                                              ),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                              Form(
                                key: _ingredientsFormKey,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(right: 10),
                                        child: TextFormField(
                                          controller: _ingredientNameController,
                                          decoration: _getInputDecoration(
                                              "Nazwa składnika"),
                                          validator: (value) =>
                                              validateTextField(value!,
                                                  'Podaj nazwe składnika'),
                                        ),
                                      ),
                                      flex: 3,
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                          controller:
                                              _ingredientAmountController,
                                          decoration:
                                              _getInputDecoration("Ilość"),
                                          validator: (value) =>
                                              validateTextField(
                                                  value!, 'Podaj ilość')),
                                      flex: 2,
                                    ),
                                    Expanded(
                                      child: Material(
                                        color: Colors.transparent,
                                        child: Center(
                                          child: Ink(
                                            decoration: const ShapeDecoration(
                                              color: Colors.green,
                                              shape: CircleBorder(),
                                            ),
                                            child: IconButton(
                                              icon: const Icon(Icons.add),
                                              color: Colors.white,
                                              onPressed: () {
                                                // print('add '+  _ingredientNameController.value.text + " - " +_ingredientAmountController.value.text);
                                                if (_ingredientsFormKey
                                                    .currentState!
                                                    .validate()) {
                                                  setState(() {
                                                    _ingredientsDish.add(Ingredient(
                                                        _ingredientsDish.length
                                                            .toString(),
                                                        _ingredientAmountController
                                                            .value.text,
                                                        _ingredientNameController
                                                            .value.text));
                                                  });
                                                  _ingredientAmountController
                                                      .clear();
                                                  _ingredientNameController
                                                      .clear();
                                                }
                                              },
                                            ),
                                            width: 40,
                                            height: 40,
                                          ),
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        // steps
                        Padding(
                          padding: EdgeInsets.only(top: 40, bottom: 20),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 15),
                                    child: Text(
                                      "Kroki przygotowania:",
                                      style: _titleStyle,
                                    ),
                                  ),
                                ],
                              ),
                              ListView.builder(
                                shrinkWrap: true,
                                physics: const BouncingScrollPhysics(),
                                itemCount: _preparationStepsDish.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                    padding: const EdgeInsets.fromLTRB(
                                        15.0, 10.0, 5.0, 10.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            (index + 1).toString() +
                                                ". " +
                                                _preparationStepsDish[index],
                                            style: _textStyle,
                                          ),
                                          flex: 5,
                                        ),
                                        Expanded(
                                          child: Material(
                                            color: Colors.white,
                                            child: Center(
                                              child: Ink(
                                                decoration:
                                                    const ShapeDecoration(
                                                  color: Colors.red,
                                                  shape: CircleBorder(),
                                                ),
                                                child: IconButton(
                                                  icon:
                                                      const Icon(Icons.delete),
                                                  color: Colors.white,
                                                  onPressed: () {
                                                    setState(() {
                                                      _preparationStepsDish
                                                          .removeAt(index);
                                                    });
                                                  },
                                                ),
                                                width: 40,
                                                height: 40,
                                              ),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                              Form(
                                key: _stepsFormKey,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: TextFormField(
                                        controller: _newStepController,
                                        decoration:
                                            _getInputDecoration("Kolejny krok"),
                                        validator: (value) => validateTextField(
                                            value!, 'Podaj opis kroku'),
                                      ),
                                      flex: 5,
                                    ),
                                    Expanded(
                                      child: Material(
                                        color: Colors.transparent,
                                        child: Center(
                                          child: Ink(
                                            decoration: const ShapeDecoration(
                                              color: Colors.green,
                                              shape: CircleBorder(),
                                            ),
                                            child: IconButton(
                                              icon: const Icon(Icons.add),
                                              color: Colors.white,
                                              onPressed: () {
                                                if (_stepsFormKey.currentState!
                                                    .validate()) {
                                                  setState(() {
                                                    _preparationStepsDish.add(
                                                        _newStepController
                                                            .value.text);
                                                  });
                                                  _newStepController.clear();
                                                }
                                              },
                                            ),
                                            width: 40,
                                            height: 40,
                                          ),
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        _createExpandableListForCategory(dishTypeCategoryList,
                            dishTypeCategorySelections, "Rodzaj dania", 0),
                        _createExpandableListForCategory(ingredientCategoryList,
                            ingredientCategorySelections, "Składniki", 1),
                        _createExpandableListForCategory(meatinessCategoryList,
                            meatinessCategorySelections, "Mięsność", 2),
                        _createExpandableListForCategory(mealTimeCategoryList,
                            mealTimeCategorySelections, "Pora posiłku", 3),
                        _createExpandableListForCategory(originCategoryList,
                            originCategorySelections, "Pochodzenie", 4),

                        Padding(
                          padding: EdgeInsets.only(top: 20, bottom: 60),
                          child: MyCircleButton(
                            "Zapisz przepis",
                            () => {
                              _addDishToFirebase()
                                  .then((_) => Navigator.pop(context)),
                            },
                            Colors.green,
                            Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _createExpandableListForCategory(List categoryValuesList,
      List<bool> selectionList, String title, int categoryType) {
    bool isAnySelected = selectionList.contains(true);
    String selectedVal = '';
    if (isAnySelected) {
      int selectedIndex = selectionList.indexOf(true);
      switch (categoryType) {
        case 0:
          DishTypeCategory selected =
              categoryValuesList[selectedIndex] as DishTypeCategory;
          selectedVal = selected.getString();
          break;
        case 1:
          IngredientsCategory selected =
              categoryValuesList[selectedIndex] as IngredientsCategory;
          selectedVal = selected.getString();
          break;
        case 2:
          MeatinessCategory selected =
              categoryValuesList[selectedIndex] as MeatinessCategory;
          selectedVal = selected.getString();
          break;
        case 3:
          MealTimeCategory selected =
              categoryValuesList[selectedIndex] as MealTimeCategory;
          selectedVal = selected.getString();
          break;
        case 4:
          OriginCategory selected =
              categoryValuesList[selectedIndex] as OriginCategory;
          selectedVal = selected.getString();
          break;
      }
    } else {
      selectedVal = '';
    }
    return ExpandableNotifier(
      child: ScrollOnExpand(
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: ExpandablePanel(
              header: Text(
                title,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              collapsed:
              isAnySelected
                  ?
              Text("Wybrano: " + selectedVal)
                  // : Text("Nie wybrano żadnej kategorii z tej grupy")
                  : Text("")
                ,
              expanded: Padding(
                padding: EdgeInsets.only(top: 21),
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      itemCount: categoryValuesList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: CheckboxListTile(
                            shape: Border.all(color: Colors.black, width: 0.5),
                            title: Text(
                                getCategoryString(categoryValuesList[index])),
                            value: selectionList[index],
                            onChanged: (bool? newVal) {
                              setState(
                                () {
                                  String changedCategoryString = '';

                                  switch (categoryType) {
                                    case 0:
                                      DishTypeCategory changed =
                                          categoryValuesList[index]
                                              as DishTypeCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 1:
                                      IngredientsCategory changed =
                                          categoryValuesList[index]
                                              as IngredientsCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 2:
                                      MeatinessCategory changed =
                                          categoryValuesList[index]
                                              as MeatinessCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 3:
                                      MealTimeCategory changed =
                                          categoryValuesList[index]
                                              as MealTimeCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 4:
                                      OriginCategory changed =
                                          categoryValuesList[index]
                                              as OriginCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                  }

                                  if (newVal != null && newVal) {
                                    if (isAnySelected) {
                                      for (int i = 0;
                                          i < selectionList.length;
                                          i++) {
                                        selectionList[i] = false;
                                        selectedCategories
                                            .remove(changedCategoryString);
                                      }
                                      selectionList[index] = newVal;
                                      selectedCategories
                                          .add(changedCategoryString);
                                    } else {
                                      isAnySelected = true;
                                      selectionList[index] = newVal;
                                      selectedCategories
                                          .add(changedCategoryString);
                                    }
                                  } else {
                                    selectionList[index] = newVal ?? false;
                                    selectedCategories
                                        .remove(changedCategoryString);
                                  }
                                  final controller =
                                      ExpandableController.of(context);
                                  controller?.toggle();
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png'],
    );
    if (result != null) {
      File file = File(result.files.single.path!);
      setState(() {
        _newImage = file;
      });
    } else {
      // User canceled the picker
    }
  }
}
