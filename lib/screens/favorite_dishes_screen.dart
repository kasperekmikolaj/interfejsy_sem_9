import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/favorite_dish.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/lists/create_expandable_list.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

import 'dish_form_screen.dart';
import 'loading_screen.dart';

class FavoriteDishesScreen extends StatefulWidget {
  @override
  _FavoriteDishesScreen createState() => _FavoriteDishesScreen();
}

class _FavoriteDishesScreen extends State<FavoriteDishesScreen> {
  late Future<User> currentUserFuture;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  Future<QuerySnapshot<FavoriteDish>> _getFavoriteDishes() async {
    String userId = await currentUserFuture.then((user) => user.id);
    var favoriteDishFirestoreRef = FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .collection('favoriteDishes')
        .withConverter<FavoriteDish>(
      fromFirestore: (snapshot, _) =>
          FavoriteDish.fromJsonFirebase(snapshot.data()!),
      toFirestore: (dish, _) => dish.toJson(),
    );
    return favoriteDishFirestoreRef.get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffffbac9),
      appBar: CustomAppBar("Twoje ulubione przepisy!", Colors.red),
      body: FutureBuilder(
        future: _getFavoriteDishes(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var querySnap = snapshot.data as QuerySnapshot<FavoriteDish>;
            List<FavoriteDish> dishes = querySnap.docs.map((e) => e.data()).toList();
            return MyCustomList(dishes, "Ulubione dania", isFavoriteDish: true, headerColor: Colors.red);
          } else {
            return LoadingScreen();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DishFormScreen(null),
          )).then((value) => setState(() => {}));
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
