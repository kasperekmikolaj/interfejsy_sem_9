import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';
import 'package:interfejsy/widgets/loading_screen.dart';
import 'package:interfejsy/widgets/user_info_container.dart';

class ProfileInfoScreen extends StatefulWidget {
  final String appBarTitle;
  final MyCircleButton button;

  ProfileInfoScreen(this.appBarTitle, this.button);

  @override
  _ProfileInfoScreenState createState() => _ProfileInfoScreenState();
}

class _ProfileInfoScreenState extends State<ProfileInfoScreen> {
  late Future<User> currentUserFuture;

  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.appBarTitle),
      ),
      body: Container(
        decoration: kInfoScreenBoxDecoration,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FutureBuilder(
                  future: currentUserFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return UserInfoColumn(
                        button: widget.button,
                        user: snapshot.data as User,
                      );
                    } else {
                      return LoadingScreen();
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
