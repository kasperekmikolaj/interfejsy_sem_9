import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/favorite_dish.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';

class ChangeNoteScreen extends StatelessWidget {
  ChangeNoteScreen(this.favoriteDish): currentNotes = favoriteDish.note;

  final FavoriteDish favoriteDish;
  String currentNotes;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white12,
        body: ListView(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          children: [
            Column(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  height: 1500,
                  decoration: kInfoScreenBoxDecoration,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 40, 20, 40),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(20, 30, 20, 0),
                          child: Text(
                            "Wprowadź notatki dla wybranego dania",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                          child: TextFormField(
                            onChanged: (String newVal) {
                              currentNotes = newVal;
                            },
                            minLines: 4,
                            maxLines: 8,
                            initialValue: favoriteDish.note,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                        MyCircleButton("Zapisz zmiany",
                            () {
                          _saveNotesChanges(context);
                        }, Colors.green, Colors.white),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _saveNotesChanges(BuildContext context) async {
    String userId = await Utils.getCurrentUserFutureFromSecureStorage().then((user) => user.id);
    var favoriteDishFirestoreRef = FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .collection('favoriteDishes')
        .withConverter<FavoriteDish>(
      fromFirestore: (snapshot, _) =>
          FavoriteDish.fromJsonFirebase(snapshot.data()!),
      toFirestore: (dish, _) => dish.toJson(),
    );
    favoriteDish.note = currentNotes;
    favoriteDishFirestoreRef.doc(favoriteDish.id).set(favoriteDish);
    Navigator.of(context).pop();
  }
}
