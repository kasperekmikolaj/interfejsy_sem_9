import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/categories/categories_enum.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/screens/dishes_screen.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

class SearchFilterScreen extends StatefulWidget {
  @override
  _SearchFilterScreen createState() => _SearchFilterScreen();
}

class _SearchFilterScreen extends State<SearchFilterScreen> {
  late Future<User> currentUserFuture;

  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  List<DishTypeCategory> dishTypeCategoryList = DishTypeCategory.values;
  List<bool> dishTypeCategorySelections =
      DishTypeCategory.values.map((e) => false).toList();

  List<IngredientsCategory> ingredientCategoryList = IngredientsCategory.values;
  List<bool> ingredientCategorySelections =
      IngredientsCategory.values.map((e) => false).toList();

  List<MeatinessCategory> meatinessCategoryList = MeatinessCategory.values;
  List<bool> meatinessCategorySelections =
      MeatinessCategory.values.map((e) => false).toList();

  List<MealTimeCategory> mealTimeCategoryList = MealTimeCategory.values;
  List<bool> mealTimeCategorySelections =
      MealTimeCategory.values.map((e) => false).toList();

  List<OriginCategory> originCategoryList = OriginCategory.values;
  List<bool> originCategorySelections =
      OriginCategory.values.map((e) => false).toList();

  List<String> selectedCategories = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar("Wyszukaj przepisy!", Colors.blue),
      body: Padding(
        padding: EdgeInsets.all(25),
        child: ListView(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          children: [
            Column(
              children: const [
                Text(
                  "Zasady korzystania z wyszukiwarki:",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 15),
                  child: Text(
                    "1. Wybierz interesującą cię kategorię a następnie zaznacz jedną z dostępnych opcji.\n"
                    "2. Możesz zaznaczyć tylko jedą opcję z danej grupy.\n3. Jeśli chcesz możesz określić wiele kategorii.",
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
            _createExpandableListForCategory(dishTypeCategoryList,
                dishTypeCategorySelections, "Rodzaj dania", 0),
            _createExpandableListForCategory(ingredientCategoryList,
                ingredientCategorySelections, "Składniki", 1),
            _createExpandableListForCategory(meatinessCategoryList,
                meatinessCategorySelections, "Mięsność", 2),
            _createExpandableListForCategory(mealTimeCategoryList,
                mealTimeCategorySelections, "Pora posiłku", 3),
            _createExpandableListForCategory(
                originCategoryList, originCategorySelections, "Pochodzenie", 4),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(30),
                  child: MyCircleButton("Wyszukaj", () {
                    Navigator.of(context).popUntil(
                      ModalRoute.withName('/'),
                    );
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (builder) => DishesScreen(selectedCategories),
                      ),
                    );
                  }, Colors.green, Colors.white),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            )
          ],
        ),
      ),
    );
  }

  _createExpandableListForCategory(List categoryValuesList,
      List<bool> selectionList, String title, int categoryType) {
    bool isAnySelected = selectionList.contains(true);
    String selectedVal = '';
    if (isAnySelected) {
      int selectedIndex = selectionList.indexOf(true);
      switch (categoryType) {
        case 0:
          DishTypeCategory selected =
              categoryValuesList[selectedIndex] as DishTypeCategory;
          selectedVal = selected.getString();
          break;
        case 1:
          IngredientsCategory selected =
              categoryValuesList[selectedIndex] as IngredientsCategory;
          selectedVal = selected.getString();
          break;
        case 2:
          MeatinessCategory selected =
              categoryValuesList[selectedIndex] as MeatinessCategory;
          selectedVal = selected.getString();
          break;
        case 3:
          MealTimeCategory selected =
              categoryValuesList[selectedIndex] as MealTimeCategory;
          selectedVal = selected.getString();
          break;
        case 4:
          OriginCategory selected =
              categoryValuesList[selectedIndex] as OriginCategory;
          selectedVal = selected.getString();
          break;
      }
    } else {
      selectedVal = '';
    }
    return ExpandableNotifier(
      child: ScrollOnExpand(
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: ExpandablePanel(
              header: Text(
                title,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              collapsed: isAnySelected
                  ? Text("Wybrano: " + selectedVal)
                  : Text("Nie wybrano żadnej kategorii z tej grupy"),
              expanded: Padding(
                padding: EdgeInsets.only(top: 21),
                child: Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      itemCount: categoryValuesList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: CheckboxListTile(
                            shape: Border.all(color: Colors.black, width: 0.5),
                            title: Text(
                                getCategoryString(categoryValuesList[index])),
                            value: selectionList[index],
                            onChanged: (bool? newVal) {
                              setState(
                                () {
                                  String changedCategoryString = '';

                                  switch (categoryType) {
                                    case 0:
                                      DishTypeCategory changed =
                                          categoryValuesList[index]
                                              as DishTypeCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 1:
                                      IngredientsCategory changed =
                                          categoryValuesList[index]
                                              as IngredientsCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 2:
                                      MeatinessCategory changed =
                                          categoryValuesList[index]
                                              as MeatinessCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 3:
                                      MealTimeCategory changed =
                                          categoryValuesList[index]
                                              as MealTimeCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                    case 4:
                                      OriginCategory changed =
                                          categoryValuesList[index]
                                              as OriginCategory;
                                      changedCategoryString =
                                          changed.getString();
                                      break;
                                  }

                                  if (newVal != null && newVal) {
                                    if (isAnySelected) {
                                      for (int i = 0;
                                          i < selectionList.length;
                                          i++) {
                                        selectionList[i] = false;
                                        selectedCategories
                                            .remove(changedCategoryString);
                                      }
                                      selectionList[index] = newVal;
                                      selectedCategories
                                          .add(changedCategoryString);
                                    } else {
                                      isAnySelected = true;
                                      selectionList[index] = newVal;
                                      selectedCategories
                                          .add(changedCategoryString);
                                    }
                                  } else {
                                    selectionList[index] = newVal ?? false;
                                    selectedCategories
                                        .remove(changedCategoryString);
                                  }
                                  final controller =
                                      ExpandableController.of(context);
                                  controller?.toggle();
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
