import 'package:flutter/material.dart';
import 'package:interfejsy/models/categories/categories_enum.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/widgets/cards/category_card.dart';
import 'package:interfejsy/widgets/buttons/menu_button.dart';
import 'package:interfejsy/widgets/stateful_appbar.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late Future<User> currentUserFuture;
  late bool isLoggedIn;

  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar("Wyszukaj przepisy!", Colors.blue),
      floatingActionButton: MenuButton(true),
      body: Container(
        padding: EdgeInsets.all(5),
        color: Colors.white60,
        child: ListView(
          children: [
            ListView.builder(
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: DishTypeCategory.values.length,
              itemBuilder: (context, index) {
                String imagePath =
                    _getImagePath(DishTypeCategory.values[index]);
                return CategoryCard(
                    DishTypeCategory.values[index].getString(), imagePath);
              },
            ),
          ],
        ),
      ),
    );
  }

  String _getImagePath(DishTypeCategory cat) {
    switch (cat) {
      case DishTypeCategory.glowne:
        return "assets/categories/pasta.jpg";
      case DishTypeCategory.maczne:
        return "assets/categories/ciasto.jpg";
      case DishTypeCategory.kanapki:
        return "assets/categories/ciastka.jpg";
      case DishTypeCategory.makarony:
        return "assets/categories/sniadanie.jpg";
      case DishTypeCategory.nalesniki:
        return "assets/categories/przekaski.jpg";
      case DishTypeCategory.pizze:
        return "assets/categories/pizza.jpg";
      case DishTypeCategory.przystawki:
        return "assets/categories/przetwory.jpg";
      case DishTypeCategory.salatki:
        return "assets/categories/salatka.jpg";
      case DishTypeCategory.sosy:
        return "assets/categories/koktajl.jpg";
      case DishTypeCategory.zapiekanki:
        return "assets/categories/grill.jpg";
      case DishTypeCategory.zupy:
        return "assets/categories/zupa.jpg";
    }
  }
}
