import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/models/dish.dart';
import 'package:interfejsy/models/ratings.dart';
import 'package:interfejsy/models/user.dart';
import 'package:interfejsy/screens/dish_details_screen.dart';
import 'package:interfejsy/utils/constants.dart';
import 'package:interfejsy/utils/utils.dart';
import 'package:interfejsy/widgets/buttons/my_circle_button.dart';

class OpinionFormScreen extends StatefulWidget {
  const OpinionFormScreen(this.user, this.dish, {Key? key}) : super(key: key);

  final User user;
  final Dish dish;

  @override
  _OpinionFormScreen createState() => _OpinionFormScreen();
}

class _OpinionFormScreen extends State<OpinionFormScreen> {
  @override
  void initState() {
    super.initState();
    currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  late Future<User> currentUserFuture;
  String currentOpinion = '';
  int _selectedRatingVal = 0;
  bool _anySelected = false;
  final List<int> _ratingValueList = [1, 2, 3, 4, 5];
  final List<bool> _checkedList = [false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white12,
        body: ListView(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          children: [
            Column(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  height: 900,
                  decoration: kInfoScreenBoxDecoration,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 40, 20, 40),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: TextFormField(
                            onChanged: (String newVal) {
                              currentOpinion = newVal;
                            },
                            minLines: 3,
                            maxLines: 6,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Twoja opinia"),
                          ),
                        ),
                        ExpandableNotifier(
                          child: ScrollOnExpand(
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: ExpandablePanel(
                                  header: Text(
                                    "Ocena",
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                    ),
                                  ),
                                  collapsed: _selectedRatingVal == 0
                                      ? Text("Nie wybrano oceny")
                                      : Text('Wybrana ocena to: ' +
                                          _selectedRatingVal.toString()),
                                  expanded: Padding(
                                    padding: EdgeInsets.only(top: 21),
                                    child: Column(
                                      children: [
                                        ListView.builder(
                                          shrinkWrap: true,
                                          physics:
                                              const BouncingScrollPhysics(),
                                          itemCount: _ratingValueList.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Padding(
                                              padding: EdgeInsets.only(top: 5),
                                              child: CheckboxListTile(
                                                shape: Border.all(
                                                    color: Colors.black,
                                                    width: 0.5),
                                                title: Text(
                                                    _ratingValueList[index]
                                                        .toString()),
                                                value: _checkedList[index],
                                                onChanged: (bool? newVal) {
                                                  setState(
                                                    () {
                                                      if (newVal != null &&
                                                          newVal) {
                                                        if (_anySelected) {
                                                          for (int i = 0;
                                                              i <
                                                                  _checkedList
                                                                      .length;
                                                              i++) {
                                                            _checkedList[i] =
                                                                false;
                                                          }
                                                          _checkedList[index] =
                                                              newVal;
                                                          _selectedRatingVal =
                                                              _ratingValueList[
                                                                  index];
                                                        } else {
                                                          _anySelected = true;
                                                          _checkedList[index] =
                                                              newVal;
                                                          _selectedRatingVal =
                                                              _ratingValueList[
                                                                  index];
                                                        }
                                                      } else {
                                                        _checkedList[index] =
                                                            newVal ?? false;
                                                        _selectedRatingVal = 0;
                                                      }
                                                    },
                                                  );
                                                },
                                              ),
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        _selectedRatingVal != 0 && currentOpinion.length > 3
                            ? Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: MyCircleButton("Zapisz ocenę",
                                    () {
                                  Rating newRating = Rating(
                                      currentOpinion,
                                      _selectedRatingVal,
                                      widget.user.displayName,
                                      widget.user.id);
                                  widget.dish.ratings.add(newRating);
                                  dishFirestoreRef
                                      .doc(widget.dish.id)
                                      .set(widget.dish);
                                  Navigator.of(context).popUntil(
                                    ModalRoute.withName('/'),
                                  );
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (builder) => DishDetailsScreen(
                                          widget.dish.id, false),
                                    ),
                                  );
                                }, Colors.green, Colors.white),
                              )
                            : Padding(
                                padding: EdgeInsets.all(20),
                                child: Text(
                                  "Aby zapisać uzupełnij pole tekstowe oraz wybierz wartość oceny.",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
