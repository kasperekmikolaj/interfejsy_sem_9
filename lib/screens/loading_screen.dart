import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:interfejsy/screens/main_screen.dart';
import 'package:interfejsy/utils/sing_in.dart';
import 'package:loading_indicator/loading_indicator.dart';

class LoadingScreen extends StatefulWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  _LoadingScreen createState() => _LoadingScreen();
}

class _LoadingScreen extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white12,
      body: LoadingIndicator(
        indicatorType: Indicator.ballClipRotateMultiple,
      ),
    );
  }
}
