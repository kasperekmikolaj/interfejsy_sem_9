class User {
  final String displayName;
  final String email;
  final String photoUrl;
  final String id;

  User(this.id, this.displayName, this.email, this.photoUrl);

  User.fromJsonSecureStorage(Map<String, dynamic> json)
      : id = json['id'],
        displayName = json['displayName'],
        email = json['email'],
        photoUrl = json['photoUrl'];

  User.fromJsonFirebase({required Map<String, dynamic> json})
      : displayName = json['displayName'],
        email = json['email'],
        photoUrl = json['photoUrl'],
        id = json['id'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'displayName': displayName,
    'email': email,
    'photoUrl': photoUrl,
  };
}