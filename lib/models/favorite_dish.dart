import 'package:interfejsy/models/abstract_dish.dart';
import 'package:interfejsy/models/dish.dart';

import 'ingridniet.dart';

class FavoriteDish implements AbstractDish {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final List<String> preparationSteps;
  @override
  final List<String> categories;
  @override
  final String preparationTime;
  @override
  final String photoUrl;
  @override
  final List<Ingredient> ingredients;

  String note;

  FavoriteDish(
      this.id,
      this.name,
      this.description,
      this.preparationSteps,
      this.categories,
      this.preparationTime,
      this.note,
      this.photoUrl,
      this.ingredients);

  FavoriteDish.fromDish(Dish dish) :
    id = dish.id,
    name = dish.name,
    description = dish.description,
    preparationSteps = dish.preparationSteps,
    categories = dish.categories,
    preparationTime = dish.preparationTime,
    photoUrl = dish.photoUrl,
    ingredients = dish.ingredients,
    note = "";


  FavoriteDish.fromJsonFirebase(Map<String, dynamic> json)
      : name = json['name'],
        description = json['description'],
        preparationSteps = json['preparationSteps'].cast<String>(),
        categories = json['categories'].cast<String>(),
        preparationTime = json['preparationTime'],
        photoUrl = json['photoUrl'],
        ingredients = json['ingredients']
            .map((elem) => Ingredient.fromJsonFirebase(json: elem))
            .toList()
            .cast<Ingredient>(),
        note = json['note'],
        id = json['id'];

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'preparationSteps': preparationSteps,
        'categories': categories,
        'preparationTime': preparationTime,
        'photoUrl': photoUrl,
        'ingredients': ingredients.map((e) => e.toJson()).toList(),
        'note': note,
      };
}
