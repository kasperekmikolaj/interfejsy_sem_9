import 'ingridniet.dart';

abstract class AbstractDish {
  final String id;
  final String name;
  final String description;
  final List<String> preparationSteps;
  final List<String> categories;
  final String preparationTime;
  final String photoUrl;
  final List<Ingredient> ingredients;

  AbstractDish(this.id, this.name, this.description, this.preparationSteps,
      this.categories, this.preparationTime, this.photoUrl, this.ingredients);

  AbstractDish.fromJsonFirebase(Map<String, dynamic> json)
      : name = json['name'],
        description = json['description'],
        preparationSteps = json['preparationSteps'].cast<String>(),
        categories = json['categories'].cast<String>(),
        preparationTime = json['preparationTime'],
        photoUrl = json['photoUrl'],
        ingredients = json['ingredients']
            .map((elem) => Ingredient.fromJsonFirebase(json: elem))
            .toList()
            .cast<Ingredient>(),
        id = json['id'];

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'preparationSteps': preparationSteps,
        'categories': categories,
        'preparationTime': preparationTime,
        'photoUrl': photoUrl,
        'ingredients': ingredients.map((e) => e.toJson()).toList(),
      };
}
