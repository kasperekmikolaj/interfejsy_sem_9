class Rating {
  final String text;
  final int stars;
  final String authorName;
  final String authorId;

  Rating(this.text, this.stars, this.authorName, this.authorId);

  Rating.fromJsonFirebase(
      {required Map<String, dynamic> json})
      : text = json['text'],
        stars = json['stars'],
        authorName = json['authorName'],
        authorId = json['authorId'];

  Map<String, dynamic> toJson() => {
    'text': text,
    'stars': stars,
    'authorName': authorName,
    'authorId': authorId,
  };
}
