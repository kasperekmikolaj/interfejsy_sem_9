import 'dart:convert';

import 'package:interfejsy/models/abstract_dish.dart';
import 'package:interfejsy/models/ratings.dart';

import 'ingridniet.dart';

class Dish implements AbstractDish {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final List<String> preparationSteps;
  @override
  final List<String> categories;
  @override
  final String preparationTime;
  @override
  final String photoUrl;
  @override
  final List<Ingredient> ingredients;

  final List<Rating> ratings;

  Dish(
      this.id,
      this.name,
      this.description,
      this.preparationSteps,
      this.categories,
      this.preparationTime,
      this.ratings,
      this.photoUrl,
      this.ingredients);

  Dish.fromJsonFirebase(Map<String, dynamic> json)
      : name = json['name'],
        description = json['description'],
        preparationSteps = json['preparationSteps'].cast<String>(),
        categories = json['categories'].cast<String>(),
        preparationTime = json['preparationTime'],
        photoUrl = json['photoUrl'],
        ingredients = json['ingredients']
            .map((elem) => Ingredient.fromJsonFirebase(json: elem))
            .toList()
            .cast<Ingredient>(),
        ratings = json.containsKey('ratings') ? json['ratings']
            .map((elem) => Rating.fromJsonFirebase(json: elem))
            .toList()
            .cast<Rating>() : [],
        id = json['id'];

  @override
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'preparationSteps': preparationSteps,
        'categories': categories,
        'preparationTime': preparationTime,
        'photoUrl': photoUrl,
        'ingredients': ingredients.map((e) => e.toJson()).toList(),
        'ratings': ratings.map((e) => e.toJson()).toList(),
      };
}
