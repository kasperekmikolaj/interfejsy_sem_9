enum DishTypeCategory {
  glowne,
  maczne,
  kanapki,
  makarony,
  nalesniki,
  pizze,
  przystawki,
  salatki,
  sosy,
  zapiekanki,
  zupy,
}

enum IngredientsCategory {
  owoce,
  warzywa,
  grzyby,
  mieso,
  ryby,
  owoceMorza,
  jajka,
  nabial,
  makaron,
  kasza,
  ryz,
}

enum MeatinessCategory {
  weganskie,
  wegetarianskie,
  miesne,
}

enum MealTimeCategory {
  sniadanie,
  przekaski,
  obiad,
  kolacja,
  lunch,
}

enum OriginCategory {
  amerykanska,
  balkanska,
  chinska,
  francuska,
  grecka,
  gruzinska,
  hiszpanska,
  indyjska,
  koreanska,
  meksykanska,
  niemiecka,
  orientalna,
  polska,
  afrykanska,
  rosyjska,
  srodziemnomorska,
  tajska,
  turecka,
  wegierska,
  wloska,
}

String getCategoryString(category) {
  String type = category.runtimeType.toString();
  switch (type) {
    case "DishTypeCategory":
      var cat = category as DishTypeCategory;
      return cat.getString();
    case "IngredientsCategory":
      var cat = category as IngredientsCategory;
      return cat.getString();
    case "MeatinessCategory":
      var cat = category as MeatinessCategory;
      return cat.getString();
    case "MealTimeCategory":
      var cat = category as MealTimeCategory;
      return cat.getString();
    case "OriginCategory":
      var cat = category as OriginCategory;
      return cat.getString();
    default:
      return "";
  }
}

extension DishTypeCategoryExtension on DishTypeCategory {
  String getString() {
    switch (this) {
      case DishTypeCategory.glowne:
        return 'Dania Główne';
      case DishTypeCategory.maczne:
        return 'Dania Mączne';
      case DishTypeCategory.kanapki:
        return 'Kanapki';
      case DishTypeCategory.makarony:
        return 'Makarony';
      case DishTypeCategory.nalesniki:
        return 'Naleśniki';
      case DishTypeCategory.pizze:
        return 'Pizze';
      case DishTypeCategory.przystawki:
        return 'Przystawki';
      case DishTypeCategory.salatki:
        return 'Sałatki';
      case DishTypeCategory.sosy:
        return 'Sosy';
      case DishTypeCategory.zapiekanki:
        return 'Zapiekanki';
      case DishTypeCategory.zupy:
        return 'Zupy';
    }
  }
}

extension IngredientsCategoryExtension on IngredientsCategory {
  String getString() {
    switch (this) {
      case IngredientsCategory.owoce:
        return 'Owoce';
      case IngredientsCategory.warzywa:
        return 'Warzywa';
      case IngredientsCategory.grzyby:
        return 'Grzyby';
      case IngredientsCategory.mieso:
        return 'Mięso';
      case IngredientsCategory.ryby:
        return 'Ryby';
      case IngredientsCategory.owoceMorza:
        return 'Owoce morza';
      case IngredientsCategory.jajka:
        return 'Jajka';
      case IngredientsCategory.nabial:
        return 'Nabiał';
      case IngredientsCategory.makaron:
        return 'Makaron';
      case IngredientsCategory.kasza:
        return 'Kasza';
      case IngredientsCategory.ryz:
        return 'Ryż';
    }
  }
}

extension MeatinessCategoryExtension on MeatinessCategory {
  String getString() {
    switch (this) {
      case MeatinessCategory.weganskie:
        return 'Wegańskie';
      case MeatinessCategory.wegetarianskie:
        return 'Wegetariańskie';
      case MeatinessCategory.miesne:
        return 'Mięsne';
    }
  }
}

extension MealTimeCategoryExtension on MealTimeCategory {
  String getString() {
    switch (this) {
      case MealTimeCategory.sniadanie:
        return 'Śniadanie';
      case MealTimeCategory.przekaski:
        return 'Przekąski';
      case MealTimeCategory.obiad:
        return 'Obiad';
      case MealTimeCategory.kolacja:
        return 'Kolacja';
      case MealTimeCategory.lunch:
        return 'Lunch';
    }
  }
}

extension OriginCategoryExtension on OriginCategory {
  String getString() {
    switch (this) {
      case OriginCategory.amerykanska:
        return 'Amerykańska';
      case OriginCategory.balkanska:
        return 'Bałkańska';
      case OriginCategory.chinska:
        return 'Chińska';
      case OriginCategory.francuska:
        return 'Francuska';
      case OriginCategory.grecka:
        return 'Grecka';
      case OriginCategory.gruzinska:
        return 'Gruzińska';
      case OriginCategory.hiszpanska:
        return 'Hiszpańska';
      case OriginCategory.indyjska:
        return 'Indyjska';
      case OriginCategory.koreanska:
        return 'Koreańska';
      case OriginCategory.meksykanska:
        return 'Meksykańska';
      case OriginCategory.niemiecka:
        return 'Niemiecka';
      case OriginCategory.orientalna:
        return 'Orientalna';
      case OriginCategory.polska:
        return 'Polska';
      case OriginCategory.afrykanska:
        return 'Afrykańska';
      case OriginCategory.rosyjska:
        return 'Rosyjska';
      case OriginCategory.srodziemnomorska:
        return 'Śródziemnomorska';
      case OriginCategory.tajska:
        return 'Tajska';
      case OriginCategory.turecka:
        return 'Turecka';
      case OriginCategory.wegierska:
        return 'Węgierska';
      case OriginCategory.wloska:
        return 'Włoska';
    }
  }
}
