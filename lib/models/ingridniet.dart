class Ingredient {
  final String id;
  final String quantity;
  final String name;

  Ingredient(this.id, this.quantity, this.name);

  Ingredient.fromJsonFirebase({required Map<String, dynamic> json})
      : id = json['id'],
        quantity = json['quantity'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'quantity': quantity,
    'name': name
  };
}